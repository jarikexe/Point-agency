<?php 

/*
*Выберает данные из базы. По правильному должно быть отдельно, но для такого маленького задание нуту смысла усложнять
*/
include("conf.php");
$conn = new mysqli(SERVER, USER_NAME, PASS, DB);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT fild1, fild2, fild3 FROM records";
$result = $conn->query($sql);

$conn->close();

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>testPoint-agency</title>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

</head>
<body>
	<!-- Стили нужно было вынести в отдельный фаил но две сьрочки .... -->
	<style>
		.record{
			margin-bottom: 20px;
			margin-top: 20px;

		}
	</style>

	<div class="container">


			
				<?php if ($result->num_rows > 0): ?>
					
					<?php while($row = $result->fetch_assoc()): ?>
					<div class="row record">
						<div class="col-3">
<?php echo $row["fild1"]; ?>
						</div>
						<div class="col-3">
<?php echo $row["fild3"]; ?>
						</div>
						<div class="col-6">
<?php echo $row["fild2"]; ?>
						</div>
					</div>
					<?php endwhile ?>

				<?php else: ?>
					<h2>no recors found</h2>
				<?php endif ?>
			

		<div class="row">
			<div class="form-group col-6">
			    <input type="text" class="form-control" id="fild1" placeholder="Short text">
			</div>
			
			<div class="form-group col-6">
			    <input type="text" class="form-control" id="fild3" placeholder="email">
			</div>
			<div class="form-group col-12">
			    <textarea class="form-control" id="fild2" placeholder="Long text"></textarea>
			</div>
			<div class="form-group col-6">
				<button id="send"  class="btn btn-primary col-6">Send</button>
			</div>
		</div>
	</div>


	<!-- js для отправки формы в обработчик .... -->

	<script>
		$("#send").click(function(){
			var fild1 = $("#fild1").val();
			var fild2 = $("#fild2").val();
			var fild3 = $("#fild3").val();


			$.ajax({  
			    type: 'POST',  
			    url: 'add.php', 
			    data: { 
			    	"fild1" : fild1,
			    	"fild2" : fild2,
			    	"fild3" : fild3,
			    },
			    success: function(response) {
			    	alert(response);
			    }
			});
		});
	</script>
</body>

</html>