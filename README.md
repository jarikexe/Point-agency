# Instalation
1. Create mysql database
...

```mysql
	mysql>	CREATE DATABASE DATABASE_NAME;
```
1. Go to **conf.php** and configure access to your database
...

```php
		define("SERVER", "localhost");
		define("USER_NAME", "user_name");
		define("PASS", "password");
		define("DB", "database_name");
```

1. Upload **records.sql** to your database
...

```bach
	sudo -p DATABASE_NAME < records.sql;
```
or u can just use **phpMyAdmin**
1. Move all the files to webserver 
1. Run index.php file from the browser 

# Usage

1. You can add and remove records using web interface